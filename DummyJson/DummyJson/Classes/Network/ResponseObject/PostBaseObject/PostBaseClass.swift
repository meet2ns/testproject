//
//  PostBaseClass.swift
//
//  Created by OneByte on 01/04/2019
//  Copyright (c) OneByte. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PostBaseClass: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let posts = "posts"
  }

  // MARK: Properties
  public var posts: [Post]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.posts].array { posts = items.map { Post(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = posts { dictionary[SerializationKeys.posts] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.posts = aDecoder.decodeObject(forKey: SerializationKeys.posts) as? [Post]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(posts, forKey: SerializationKeys.posts)
  }

}
