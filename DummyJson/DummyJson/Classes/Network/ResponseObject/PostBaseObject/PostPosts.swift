//
//  PostPosts.swift
//
//  Created by OneByte on 01/04/2019
//  Copyright (c) OneByte. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Post: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let isVideo = "isVideo"
    static let image = "image"
    static let text = "text"
    static let url = "url"
  }

  // MARK: Properties
  public var isVideo: Bool? = false
  public var image: String?
  public var text: String?
  public var url: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    isVideo = json[SerializationKeys.isVideo].boolValue
    image = json[SerializationKeys.image].string
    text = json[SerializationKeys.text].string
    url = json[SerializationKeys.url].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.isVideo] = isVideo
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = text { dictionary[SerializationKeys.text] = value }
    if let value = url { dictionary[SerializationKeys.url] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.isVideo = aDecoder.decodeBool(forKey: SerializationKeys.isVideo)
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.text = aDecoder.decodeObject(forKey: SerializationKeys.text) as? String
    self.url = aDecoder.decodeObject(forKey: SerializationKeys.url) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(isVideo, forKey: SerializationKeys.isVideo)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(text, forKey: SerializationKeys.text)
    aCoder.encode(url, forKey: SerializationKeys.url)
  }

}
