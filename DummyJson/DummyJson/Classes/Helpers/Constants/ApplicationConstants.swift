
//
//  ApplicationConstants.swift
//
//
//  Created by iOS Team on 6/9/17.
//  Copyright © 2019 iOS Team. All rights reserved.
//

import Foundation
import UIKit

struct ApplicationNibStringConstants {
    // MARK: Navigation Bar
    static let kHomeViewController  = "HomeViewController"
    static let kWebViewController = "WebViewController"
}

struct ApplicationsColorsConstants {
    
    //MARK: Navigation Bar
    static let kNavigationTitleColor = UIColor.white
    static let kNavigationTitleBackGroundColor = UIColor.clear
    static let kNavigationBarBackGroundColor = UIColor.black
    static let kNavigationBarTintColor = UIColor.white
    
}


struct ApplicationIdentifierConstants {
    static let kHomeCell = "HomeCell"
}


struct ApplicationNavigationsTitles {
    
    static let kHomeNavigationTitle = "Home"

}

struct ApplicationStringConstants {
    static let kEmptyCharacter = "\u{200B}"
}

struct ApplicationAlertMessages {
    // MARK: Common
    static let kAlertTitle = "Alert"
    
}

