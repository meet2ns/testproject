//
//  SignupViewController.swift
//  CelebrityApp
//
//  Created by Onebyte on 1/22/19.
//  Copyright © 2019 onebyte. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKNavigationDelegate {

    //MARK: IBOutlets
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var titleView: UIView!
    @IBOutlet var webView: WKWebView!
    
    //MARK: Variables
    var titleText = ""
    var webUrl = ""

    //MARK: Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.text = titleText
        webView.frame = view.bounds
        webView.navigationDelegate = self
        
        let url = URL(string: webUrl)!
        let urlRequest = URLRequest(url: url)
        
        webView.load(urlRequest)
        webView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        self.configureBarButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    //MARK: NavigationBar Methods
    func configureBarButtons() -> Void{
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "white-back"), style: .plain, target: self, action: #selector(backButtonTapped))
        
        //        init(coder: (named: UIImage(named: "white-back"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }

    //MARK: WebView
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url,
                let host = url.host, !host.hasPrefix("www.google.com"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                print(url)
//                print("Redirected to browser. No need to open it locally")
                decisionHandler(.cancel)
            } else {
//                print("Open it locally")
                decisionHandler(.allow)
            }
        } else {
//            print("not a user click")
            decisionHandler(.allow)
        }
    }
    
    //MARK: Private Methods
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK: Action Methods
    @IBAction func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}
