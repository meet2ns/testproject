//
//  HistoryDatasource.swift
//  CarMate
//
//  Created by Nauman Saleem on 27/05/2018.
//  Copyright © 2018 iDevz. All rights reserved.
//

import UIKit
import AVKit

class HomeDatasource: NSObject , UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    public var postArray = [Post]()
    public var postPlayArray = [Int]()

    //MARK: Callbacks
    public var didReceiveSelectedIndexCallback: ((_ url:String) -> Void)?
    public var didReceiveReplyIndexCallback: ((_ indexRow:IndexPath) -> Void)?
    
    //MARK: TableView Delegate/Datasource
    override init() {
        super.init()

    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.tableView(_:tableView, cellForDashBoardJobRowAt:indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForDashBoardJobRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = ApplicationIdentifierConstants.kHomeCell
        var cell: HomeCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeCell
        if cell == nil {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeCell
        }

        cell.selectionStyle = .none
        cell.tableView = tableView
        cell.setContent(post: self.postArray[indexPath.row])
        
        cell.didReceiveReplyIndexCallback = {
            let postObject = self.postArray[indexPath.row]
            if let callBack = self.didReceiveSelectedIndexCallback {
                if postObject.isVideo ?? false {
                    callBack(postObject.url ?? "")
                }
            }
        }
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = (cell as? HomeCell) else { return };
        let visibleCells = tableView.visibleCells;
        let minIndex = visibleCells.startIndex;
        if tableView.visibleCells.index(of: cell) == minIndex {
            videoCell.playerView.player?.play();
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = cell as? HomeCell else { return };
        
        videoCell.playerView.player?.pause();
        videoCell.playerView.player = nil;
    }
}

