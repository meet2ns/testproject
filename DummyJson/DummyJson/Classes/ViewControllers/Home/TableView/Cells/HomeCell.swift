//
//  HistoryCell.swift
//  CarMate
//
//  Created by Nauman Saleem on 27/05/2018.
//  Copyright © 2018 iDevz. All rights reserved.
//

import UIKit
import AVFoundation
import WebKit

class HomeCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postText: UILabel!
    @IBOutlet weak var postVideoView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var playerVideoView: UIView!
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    //MARK: Variables
    var tableView: UITableView!
    var videoUrl = ""
    var avPlayer: AVPlayer!
    
    //MARK: Callbacks
    public var didReceiveReplyIndexCallback: (() -> Void)?
    
    //MARK: Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Private Methods
    func setContent(post: Post) {
        self.setView(view: self.playerVideoView, hidden: true)

        if post.isVideo ?? false{
            self.postVideoView.isHidden = false
            self.postImage.image = UIImage(named: post.image ?? "")
            self.videoUrl = post.url ?? ""
            self.imageHeightConstraint.constant = 250
        }else {
            self.postVideoView.isHidden = true
            self.setView(view: self.playerVideoView, hidden: true)
            self.postImage.image = UIImage(named: post.image ?? "")
            if let myImage = UIImage(named: post.image ?? "") {
                let myImageWidth = myImage.size.width
                let myImageHeight = myImage.size.height
                let myViewWidth = self.postImage.frame.size.width
                
                let ratio = myViewWidth/myImageWidth
                let scaledHeight = myImageHeight * ratio
                imageHeightConstraint.constant = scaledHeight
                self.postImage.image = UIImage(named: post.image ?? "")
            }
        }
        self.postText.text = post.text
    }
    
    private func playVideo(from file:String) {
        let file = file.components(separatedBy: ".")

        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        
        let soundURL = NSURL(fileURLWithPath: path)
        avPlayer = AVPlayer(url: soundURL as URL);
        self.playerView?.playerLayer.player = avPlayer;
        self.playerView.frame = self.playerVideoView.frame
        avPlayer.play()
    }
    
    
    func setView(view: UIView, hidden: Bool) {
        if self.avPlayer != nil && hidden{
            self.avPlayer.pause()
        }
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    //MARK: Action Methods
    @IBAction func pauseButtonTapped(_ sender: Any) {
        self.setView(view: self.playerVideoView, hidden: true)
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        self.setView(view: self.playerVideoView, hidden: false)
        self.playVideo(from: self.videoUrl)
    }
   
}
