


import UIKit

class HomeView: UIView {

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableBackgroundView: UIView!
    @IBOutlet weak var noVoucherLbl: UILabel!
    
    //MARK: Variables
    public var dashBoardDatasource: HomeDatasource!


}
