//

// Vourity

//  Created by Muneeb Ali on 6/20/18.

//  Copyright © 2018 Vourity AB. All rights reserved.


import UIKit
import AVFoundation
import AVKit

class HomeViewController: BaseViewController {
    
    //MARK: IBOutlets
    @IBOutlet var homeView: HomeView!
    @IBOutlet var proposalView: UIView!
    @IBOutlet var proposalTextField: UITextField!
    
    //MARK: Variables
    var startPoint:CGPoint?
    
    //MARK: Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLatestJobs()
    }
    
    //MARK: NavigationBar
    override func configureNavigationBarTitle() {
        self.title = ApplicationNavigationsTitles.kHomeNavigationTitle
    }
    
    //MARK: Private MEthods
    private func playVideo(from file:String) {
        let file = file.components(separatedBy: ".")
        
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player.play()
    }

    //MARK: Player
    func handlePlayerButton(videoUrl: String) {
        
        guard let path = Bundle.main.path(forResource: videoUrl, ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }

        let videoURL = URL(string: path)
        let player = AVPlayer(url: videoURL!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            playerController.player!.play()
        }
    }
    
    // MARK: Configure DataSource
    override func configureDataSource() -> Void {
        self.homeView.dashBoardDatasource = HomeDatasource()
        self.homeView.tableView.dataSource = self.homeView.dashBoardDatasource
        self.homeView.tableView.delegate = self.homeView.dashBoardDatasource
        self.homeView.tableView.backgroundView = self.homeView.tableBackgroundView
        self.homeView.tableView.tableFooterView = UIView.init()
        self.homeView.dashBoardDatasource.didReceiveSelectedIndexCallback = { url in
        }
        self.homeView.tableView.reloadData()
    }
    
    //MARK: API    
    func getLatestJobs() {
        if let path = Bundle.main.path(forResource: "document", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe)
                do {
                    let jsonResult: NSDictionary = try JSONSerialization.jsonObject(with: jsonData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    let reponceObject = PostBaseClass(object: jsonResult)
                    if let posts = reponceObject.posts {
                        self.homeView.dashBoardDatasource.postArray = posts
                        self.homeView.tableView.reloadData()
                    }
                } catch {}
            } catch {}
        }
    }
}


