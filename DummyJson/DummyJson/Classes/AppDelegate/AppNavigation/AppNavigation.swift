//
//  AppNavigation.swift
//  CodingChallenge
//
//  Created by OneByte on 5/6/18.
//  Copyright © 2018 iDevz. All rights reserved.
//

import Foundation
import UIKit


extension AppDelegate{
    func moveToHomeVC() {
        let viewController = UINavigationController(rootViewController: HomeViewController(nibName: ApplicationNibStringConstants.kHomeViewController, bundle: nil))
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }

    func showAlert(title: String, message: String) {
        let myAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        myAlert.addAction(UIAlertAction(title: ("OK"), style: .default, handler:  { action in
        }))
        self.window?.rootViewController?.present(myAlert, animated: false, completion: nil)
        
    }
    
}

